﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleClass : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//Ho mostrarà una sola vegada
		Debug.Log ("Hola des de l'Start!");
	}
	
	// Update is called once per frame
	void Update () {
		//Ho mostrarà una vegada per frame
		Debug.Log ("Hola des de l'update!");
	}

	void OnEnable(){
		//Es mostrarà quan s'activi l'script
		Debug.Log ("Hola des de l'OnEnable");
	}

	void OnDisable(){
		//Es mostrarà quan es deshabiliti l'script
		Debug.Log ("Hola des de l'OnDisable");
	}
}
