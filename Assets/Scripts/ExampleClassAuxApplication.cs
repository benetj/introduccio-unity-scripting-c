﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleClassAuxApplication : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//Ens mostrarà per Console el nom de la plataforma que estam executant el joc
		Debug.Log ("La plataforma es " + Application.platform);

		switch (Application.platform) {
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.OSXPlayer:
				Debug.Log ("MacOs");
				break;
			case RuntimePlatform.WindowsEditor:
			case RuntimePlatform.WindowsPlayer:
				Debug.Log ("Windows");
				break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
