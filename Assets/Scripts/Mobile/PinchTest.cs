﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchTest : MonoBehaviour {

	public float perspectiveZoomSpeed = 0.5f;
	public float orthoZoomSpeed = 0.5f;
	private Camera myCamera;

	// Use this for initialization
	void Start () {
		myCamera = GetComponent<Camera> ();
		PinchDetector.PinchAction += PinchDetector_PinchAction;
	}

	void PinchDetector_PinchAction (float deltaMagnitudeDiff)
	{
		// Segons si la càmera es ortho o perspectiva, farem coses diferents
		if (myCamera.orthographic) {
			// Canviam el tamany de la càmera
			myCamera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
			//Ens asseguram que estam a un rang adecuat
			myCamera.orthographicSize = Mathf.Max (myCamera.orthographicSize, 0.1f);
		} else {
			// Canviam el camp de visió
			myCamera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
			// Ens asseguram que estam a un rang adecuat
			myCamera.fieldOfView = Mathf.Clamp (myCamera.fieldOfView, 0.1f, 179.9f);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
