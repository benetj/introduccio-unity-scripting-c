﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationServiceTest : MonoBehaviour {
	private bool isInitialized = false;

	// Definició del mètode Start com a Coroutine https://docs.unity3d.com/ScriptReference/Coroutine.html
	IEnumerator Start (){
		if (!Input.location.isEnabledByUser)
			yield break;
		Input.location.Start ();
		int maxWait = 20;
		// Esperam a que inicialitzi
		while(Input.location.status == LocationServiceStatus.Initializing && maxWait > 0){
			yield return new WaitForSeconds (1);
			maxWait--;
		}
		// Si no ha pogut inicialitzar en 20 segons alguna cosa ha anat malament
		if(maxWait < 1){
			Debug.Log ("Error, no s'ha pogut iniciar");
			yield break;
		}

		// Si alguna cosa ha anat malament acabam l'script
		if (Input.location.status == LocationServiceStatus.Failed) {
			Debug.Log ("Error, no s'ha pogut localitzar el dispositiu");
			yield break;
		} else {
			isInitialized = true;
		}
	}

	void Update(){
		if (isInitialized) {
			Debug.Log ("Latitut: " + Input.location.lastData.latitude);
			Debug.Log ("Logitut: " + Input.location.lastData.longitude);
			Debug.Log ("Altitut: " + Input.location.lastData.altitude);
		}
	}

	void OnDestroy(){
		if (isInitialized) {
			Input.location.Stop ();
		}
	}

}
