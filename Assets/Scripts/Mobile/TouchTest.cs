﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchTest : MonoBehaviour {

	public Text text;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		string frase = "Tocant la pantalla amb " + Input.touchCount + " dits";
		text.text = frase;
		Debug.Log (frase);
	}
}
