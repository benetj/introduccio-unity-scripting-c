﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// Farem girar l'objecte segons el giroscopi del dispositiu
		transform.rotation = Input.gyro.attitude;
	}
}
