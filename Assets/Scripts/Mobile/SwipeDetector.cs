﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SwipeDetector : MonoBehaviour {

	public enum SwipeDirection
	{
		Up, Down, Right, Left
	}

	public static event Action<SwipeDirection> SwipeAction;
	private bool swiping = false;
	private bool eventSent = false;
	private Vector2 lastPosition;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount == 0)
			return;
		if (Input.GetTouch (0).deltaPosition.sqrMagnitude != 0) {
			if (!swiping) {
				swiping = true;
				lastPosition = Input.GetTouch (0).position;
				return;
			} else {
				if (!eventSent) {
					if (SwipeAction != null) {
						Vector2 direction = Input.GetTouch (0).position - lastPosition;
						if (Mathf.Abs (direction.x) > Mathf.Abs (direction.y)) {
							if (direction.x > 0) {
								SwipeAction (SwipeDirection.Right);
							} else {
								SwipeAction (SwipeDirection.Left);
							}
						} else {
							if (direction.y > 0) {
								SwipeAction (SwipeDirection.Up);
							} else {
								SwipeAction (SwipeDirection.Down);
							}
						}
						eventSent = true;
					}
				}
			}
		} else {
			swiping = false;
			eventSent = false;
		}
	}
}
