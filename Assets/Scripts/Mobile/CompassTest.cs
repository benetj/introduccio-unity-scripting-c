﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompassTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// Activació del servei de localització
		Input.location.Start ();
	}
	
	// Update is called once per frame
	void Update () {
		// Farem que l'objecte giri cap al nord geogràfic
		transform.rotation = Quaternion.Euler (0, -Input.compass.trueHeading, 0);

		// En cas de voler apuntar al nord magètic usarem Input.compass.magneticHeading
	}
}
