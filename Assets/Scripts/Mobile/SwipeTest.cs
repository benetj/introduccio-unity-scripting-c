﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SwipeDetector.SwipeAction += SwipeDetector_SwipeAction;

	}

	void SwipeDetector_SwipeAction (SwipeDetector.SwipeDirection direction)
	{
		switch (direction) {
		case SwipeDetector.SwipeDirection.Left:
				Debug.Log ("Swipe Esquerra");
				break;
		case SwipeDetector.SwipeDirection.Right:
			Debug.Log ("Swipe Dreta");
				break;
		case SwipeDetector.SwipeDirection.Up:
			Debug.Log ("Swipe Amunt");
				break;
		case SwipeDetector.SwipeDirection.Down:
			Debug.Log ("Swipe Avall");
				break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
