﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleClassInputs : MonoBehaviour {
	//Atributs de l'script que podrem manipular des de l'editor
	public int provaInt;
	public string provaString;
	public float provaFloat;
	public bool provaBool;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Cada frame es mostrará el valor dels atributs
		Debug.Log ("provaInt " + provaInt);
		Debug.Log ("provaString " + provaString);
		Debug.Log ("provaFloat " + provaFloat);
		Debug.Log ("provaBool " + provaBool);
	}
}
