﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleClassAuxInput : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		//KeyCode conté constants que defineixen cada una de les tecles
		if (Input.GetKeyUp(KeyCode.Space)) {
			Debug.Log ("Espai!!");
		}


		if (Input.touchCount > 0 && Input.GetTouch (0).phase == TouchPhase.Moved) {
			Debug.Log ("Has tocat la pantalla!");
		}

	}
}
