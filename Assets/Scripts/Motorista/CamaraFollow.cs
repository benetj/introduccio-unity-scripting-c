﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraFollow : MonoBehaviour {
	public Transform transformMotorista;
	
	// Update is called once per frame
	void Update () {
		transform.position = transformMotorista.position - Vector3.forward * 10;
	}
}
