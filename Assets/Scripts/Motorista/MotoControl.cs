﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotoControl : MonoBehaviour {
	public float velocidadRotacion = 50;
	public float velocidadLineal=1;
	public Transform ruedaTrasera;
	public event eliminadoDelegate eliminado;
	public delegate void eliminadoDelegate();

	private Rigidbody2D motorigidbody;
	private float radiorueda;

	// Use this for initialization
	void Start () {
		motorigidbody = GetComponent<Rigidbody2D>();
		radiorueda = GetComponent<CircleCollider2D> ().radius + 0.1f;

		Debug.Log ("Radio roda: " + radiorueda);
	}

	public void MueveDerecha(){
		Debug.Log ("Toca en terra" + TocaElSuelo());
		if (TocaElSuelo ()) {
			Debug.Log (transform.right.x);
			//motorigidbody.velocity += new Vector2 (transform.right.x * velocidadLineal, transform.right.y * velocidadLineal) * Time.deltaTime;
			motorigidbody.position += Vector2.right * velocidadLineal * Time.deltaTime;
			//Debug.Log ("Dreta: " + new Vector2 (transform.right.x * velocidadLineal, transform.right.y * velocidadLineal) * Time.deltaTime);
		}
	}

	public void MueveIzquierda(){
		Debug.Log ("Toca en terra" + TocaElSuelo());
		if (TocaElSuelo ()) {
			//motorigidbody.velocity -= new Vector2 (transform.right.x * velocidadLineal, transform.right.y * velocidadLineal) * Time.deltaTime;
			motorigidbody.position += Vector2.left * velocidadLineal * Time.deltaTime;
		}
	}

	public void RotaDerecha(){
		motorigidbody.MoveRotation (motorigidbody.rotation - velocidadRotacion * Time.deltaTime);
	}

	public void RotaIzquierda(){
		motorigidbody.MoveRotation (motorigidbody.rotation + velocidadRotacion * Time.deltaTime);
	}


	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.LeftArrow)) {
			MueveIzquierda ();
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			MueveDerecha ();
		}

		if (Input.GetKey (KeyCode.UpArrow)) {
			RotaDerecha ();
		}

		if (Input.GetKey (KeyCode.DownArrow)) {
			RotaIzquierda ();
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject != gameObject) {
			if (eliminado != null)
				eliminado ();
		}
	}

	bool TocaElSuelo(){
		return (Physics2D.OverlapCircleAll (ruedaTrasera.position, radiorueda).Length > 0);
	}
}
