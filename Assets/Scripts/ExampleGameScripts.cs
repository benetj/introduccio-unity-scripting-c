﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleGameScripts : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SortirJoc(){
		//Sortirà del joc
		Debug.Log("sortida del joc");
		Application.Quit ();
	}

	public void NovaPartida(){
		//Es passa per paràmetre el nom de l'escena a carregar
		UnityEngine.SceneManagement.SceneManager.LoadScene ("NuevaEscena");
	}
}
